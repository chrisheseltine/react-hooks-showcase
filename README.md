# About
This project was bootstrapped with [Create React App](https://github.com/facebook/create-react-app), [Typescript](https://create-react-app.dev/docs/adding-typescript/#getting-started-with-typescript-and-react) and [React Bootstrap](https://react-bootstrap.github.io/).

# Setup
1. Clone the repo locally
2. In terminal move to repo dir and `npm install`
3. `npm start`, `npm run start` or `npm run dev` to run in development mode
4. `npm run test` to test
5. `npm run build` to build

# Authors
- [Christopher Heseltine](https://www.heseltech.uk/)