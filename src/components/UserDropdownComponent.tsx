import React, { ReactElement, useContext, FC } from 'react'
import { Dropdown } from 'react-bootstrap'
import DropdownToggle from 'react-bootstrap/DropdownToggle'
import { UsersContext, UsernameDropdownFilteredUsernameContext } from '../views/RootView';
import User from '../models/UserModel';

interface Props {
  onChangeUsernameDropdown: (e: React.MouseEvent<HTMLInputElement, MouseEvent>) => void
}

/**
 * A dropdown for filtering the user table by username.
 * @param {Props} props object containing: onChangeUsernameDropdown as (e: React.MouseEvent<HTMLInputElement, MouseEvent>) => void
 * @returns {ReactElement} a username dropdown.
 */
const UserDropdownComponent: FC<Props> = (props: Props): ReactElement => {
  const users = useContext(UsersContext) as User[]
  const usernameDropdownFilteredUsername = useContext(UsernameDropdownFilteredUsernameContext)
  return (
    <Dropdown>
      <DropdownToggle variant='success' style={{ fontWeight: 'bold' }}>
        {usernameDropdownFilteredUsername === 'undefined' ? 'Show All ' : usernameDropdownFilteredUsername + ' '}
      </DropdownToggle>
      <Dropdown.Menu>
        <Dropdown.Item
          onClick={(e: React.MouseEvent<HTMLInputElement, MouseEvent>) => props.onChangeUsernameDropdown(e)}
          id={'undefined'}
        >
          Show All
        </Dropdown.Item>
        {users && users.map(user => (
          <Dropdown.Item
            key={user.username}
            onClick={(e: React.MouseEvent<HTMLInputElement, MouseEvent>) => props.onChangeUsernameDropdown(e)}
            id={user.username}
          >
            {user ? user.username : 'Error: Username not found!'}
          </Dropdown.Item>
        ))}
      </Dropdown.Menu>
    </Dropdown >
  )
}

export default UserDropdownComponent



