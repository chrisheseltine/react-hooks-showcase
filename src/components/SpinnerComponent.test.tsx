import { render, screen } from '@testing-library/react'
import SpinnerComponent from './SpinnerComponent'


it('renders without error', () => {
  render(<SpinnerComponent />)
})

it('renders all children', () => {
  render(<SpinnerComponent />)
  const parentElement = screen.getByTestId('spinner-component-root-container')
  expect(parentElement).toContainElement(screen.getByTestId('spinner-component-spinner'))
})