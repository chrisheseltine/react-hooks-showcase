import { render, screen } from '@testing-library/react'
import HeaderComponent from './HeaderComponent'


it('renders without error', () => {
  render(<HeaderComponent />)
})

it('renders the header title with the correct text', () => {
  render(<HeaderComponent />)
  expect(screen.getByTestId('header-component-title')).toHaveTextContent(/social user management system/i)
  expect(screen.getByTestId('header-component-subtitle')).toHaveTextContent(/version/i)
})