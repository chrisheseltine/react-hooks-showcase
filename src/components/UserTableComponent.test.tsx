import { render, screen } from '@testing-library/react'
import UserTableComponent from './UserTableComponent'
import { TEST_USERS, TEST_FUNCTION } from '../tests/testdata'

it('renders without error', () => {
  render(
    <UserTableComponent
      users={TEST_USERS}
      onChangeUsernameDropdown={TEST_FUNCTION}
    />
  )
})

it('displays the correct column names', () => {
  render(
    <UserTableComponent
      users={TEST_USERS}
      onChangeUsernameDropdown={TEST_FUNCTION}
    />
  )
  expect(screen.getByTestId('user-table-column-header-id')).toHaveTextContent(/id/i)
  expect(screen.getByTestId('user-table-column-header-name')).toHaveTextContent(/name/i)
  expect(screen.getByTestId('user-table-column-header-username')).toHaveTextContent(/username/i)
  expect(screen.getByTestId('user-table-column-header-email')).toHaveTextContent(/email/i)
  expect(screen.getByTestId('user-table-column-header-phone')).toHaveTextContent(/phone/i)
  expect(screen.getByTestId('user-table-column-header-website')).toHaveTextContent(/website/i)
})

it('displays the correct number of rows, based on user length', () => {
  const twoUsers = TEST_USERS.slice(0, 2)
  render(
    <UserTableComponent
      users={twoUsers}
      onChangeUsernameDropdown={TEST_FUNCTION}
    />
  )
  expect(screen.getAllByTestId('user-table-data-row').length).toBe(2)
})

it('populates each data cell with valid, non-empty data, given such input', () => {
  const oneUser = [TEST_USERS[0]]
  render(
    <UserTableComponent
      users={oneUser}
      onChangeUsernameDropdown={TEST_FUNCTION}
    />
  )
  const dataCells = screen.getAllByTestId('user-table-data-cell')
  for (let i = 0; i < dataCells.length; i++) {
    expect(dataCells[i]).not.toBeEmptyDOMElement()
  }
})


