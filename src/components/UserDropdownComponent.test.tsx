import { render } from '@testing-library/react'
import UserDropdownComponent from './UserDropdownComponent'
import { TEST_FUNCTION } from '../tests/testdata';


it('renders without error', () => {
  render(<UserDropdownComponent onChangeUsernameDropdown={TEST_FUNCTION} />)
})