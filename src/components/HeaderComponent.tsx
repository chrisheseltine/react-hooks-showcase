import { ReactElement, FC } from 'react'
import { Col, Row } from 'react-bootstrap'
import packageJSON from '../../package.json'

/**
 * A header for housing the title and other relevant information.
 * @returns {ReactElement} a header for the app.
 */
const HeaderComponent: FC = (): ReactElement => {
  return (
    <header data-testid='header-component-header'>
      <br />
      <Row>
        <Col xs="1" />
        <Col xs="auto">
          <h1 data-testid='header-component-title'>
            Social User Management System
          </h1>
          <h3 data-testid='header-component-subtitle'>
            Version {packageJSON.version}
          </h3>
        </Col>
        <Col />
      </Row>
    </header>
  )
}

export default HeaderComponent



