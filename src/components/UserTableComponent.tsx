import React, { ReactElement, FC } from 'react'
import { Row, Col, Table } from 'react-bootstrap'
import User from '../models/UserModel'
import UserDropdownComponent from './UserDropdownComponent'

interface Props {
  users: User[],
  onChangeUsernameDropdown: (e: React.MouseEvent<HTMLInputElement, MouseEvent>) => void
}

/**
 * A table for displaying a collection of the User model.
 * @param {Props} props object containing: users as User[], onChangeUsernameDropdown as (e: React.MouseEvent<HTMLInputElement, MouseEvent>) => void
 * @returns {ReactElement} a user table.
 */
const UserTableComponent: FC<Props> = (props: Props): ReactElement => {
  return (
    <>
      <Row>
        <Col />
        <Col xs='auto'>
          <UserDropdownComponent onChangeUsernameDropdown={props.onChangeUsernameDropdown} />
        </Col>
        <Col xs="1" />
      </Row>
      <br />
      <Table
        striped
        bordered
        hover
        data-testid="user-table-component-table"
      >
        <thead>
          <tr>
            <th
              scope="col"
              data-testid="user-table-column-header-id"
            >
              ID
            </th>
            <th
              scope="col"
              data-testid="user-table-column-header-name"
            >
              Name
            </th>
            <th
              scope="col"
              data-testid="user-table-column-header-username"
            >
              Username
            </th>
            <th
              scope="col"
              data-testid="user-table-column-header-email"
            >
              Email
            </th>
            <th
              scope="col"
              data-testid="user-table-column-header-phone"
            >
              Phone
            </th>
            <th
              scope="col"
              data-testid="user-table-column-header-website"
            >
              Website
            </th>
          </tr>
        </thead>
        <tbody>
          {props.users.map((user, i) => {
            return (
              <tr
                key={`user-table-row-${i}`}
                data-testid='user-table-data-row'
              >
                <th
                  scope="row"
                  data-testid='user-table-data-cell'
                >
                  {user.id}
                </th>
                <td data-testid='user-table-data-cell'>
                  {user.name}
                </td>
                <td data-testid='user-table-data-cell'>
                  {user.username}
                </td>
                <td data-testid='user-table-data-cell'>
                  {user.email}
                </td>
                <td data-testid='user-table-data-cell'>
                  {user.phone}
                </td>
                <td data-testid='user-table-data-cell'>
                  {user.website}
                </td>
              </tr>
            )
          })}
        </tbody>
      </Table>
    </>
  )
}

export default UserTableComponent



