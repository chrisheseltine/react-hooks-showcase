import { ReactElement, FC } from 'react'
import { Container, Col, Row, Spinner } from 'react-bootstrap'

/**
 * A spinner for keeping the UI active whilst waiting for HTTP requests (or any other loading process) to complete.
 * @returns {ReactElement} a loading spinner.
 */
const SpinnerComponent: FC = (): ReactElement => {
  return (
    <Container
      className='spinner-component-root-container'
      data-testid='spinner-component-root-container'
    >
      <Row>
        <Col />
        <Col xs='auto'>
          <Spinner
            animation='grow'
            variant='dark'
            data-testid='spinner-component-spinner'
          />
        </Col>
        <Col />
      </Row>
      <Row>
        <Col />
        <Col xs='auto'>
          <span>Loading...</span>
        </Col>
        <Col />
      </Row>
    </Container>
  )
}

export default SpinnerComponent



