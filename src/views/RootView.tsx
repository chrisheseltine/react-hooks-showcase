
import React, { ReactElement, useEffect, useState, FC } from 'react'
import HeaderComponent from '../components/HeaderComponent'
import UserTableComponent from '../components/UserTableComponent'
import getUsers from '../api/getUsers'
import User from '../models/UserModel'
import SpinnerComponent from '../components/SpinnerComponent'

export const UsersContext = React.createContext<User[] | null>(null)
export const UsernameDropdownFilteredUsernameContext = React.createContext<string>('undefined')

/**
 * The root view (top-level) of the app.
 * @returns {ReactElement} the root view.
 */
const RootView: FC = (): ReactElement => {
  const [users, setUsers] = useState<User[] | null>(null)
  const [usernameDropdownFilteredUsername, setUsernameDropdownFilteredUsername] = useState<string>('undefined')

  useEffect(() => {
    getUsers()
      .then(res => res.data as User[])
      .then(users => setUsers(users))
      .catch(err => console.log(err))
  }, [])

  /**
   * Handles events emitted from username dropdown.
   * @param {React.MouseEvent<HTMLInputElement, MouseEvent>} e The raw event that is fired from the HTML element.
   */
  const onChangeUsernameDropdown = (e: React.MouseEvent<HTMLInputElement, MouseEvent>) => {
    setUsernameDropdownFilteredUsername((e.target as HTMLInputElement).id)
  }

  return (
    <div data-testid="root-view-root-div">
      <HeaderComponent />
      <br />
      {users ? (
        <UsersContext.Provider value={users}>
          <UsernameDropdownFilteredUsernameContext.Provider value={usernameDropdownFilteredUsername}>
            <UserTableComponent
              users={usernameDropdownFilteredUsername === 'undefined'
                ? users
                : users.filter(user => user.username === usernameDropdownFilteredUsername)
              }
              onChangeUsernameDropdown={onChangeUsernameDropdown}
            />
          </UsernameDropdownFilteredUsernameContext.Provider>
        </UsersContext.Provider>
      ) : (
        <SpinnerComponent />
      )}
    </div>
  )
}

export default RootView
