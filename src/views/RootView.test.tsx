import { render, screen } from '@testing-library/react'
import App from './RootView'


it('renders without error', () => {
  render(<App />)
})

it('renders all children in initial state', () => {
  render(<App />)
  const parentElement = screen.getByTestId('root-view-root-div')
  expect(parentElement).toContainElement(screen.getByTestId('header-component-header'))
  // expect(parentElement).toContainElement(screen.getByTestId('spinner-component-root-container'))
})

/* it('renders all children after network calls', () => {
  render(<App />)
  const parentElement = screen.getByTestId('root-view-root-div')
  // Mock the network call to fulfill the stateful data (users)
  expect(parentElement).toContainElement(screen.getByTestId('header-component-header'))
}) */