import User from '../models/UserModel'

export const TEST_USERS: User[] = [{
  id: 1,
  name: 'test1',
  username: 'test1',
  email: 'test1@debug.com',
  address: {
    street: '123 Test Street',
    suite: 'Test Suite',
    city: 'Testville',
    zipcode: 'ES67RT',
    geo: {
      lat: 0.12,
      lng: -1.65
    }
  },
  phone: '+447672625784',
  website: 'testerplus.com',
  company: {
    name: 'TesterPlus',
    catchPhrase: 'Test company for the world to see',
    bs: 'Smashing tests since 1990!'
  }
},
{
  id: 2,
  name: 'test2',
  username: 'test2',
  email: 'test2@debug.com',
  address: {
    street: '456 Debug Street',
    suite: 'Debug Suite',
    city: 'Debugville',
    zipcode: 'ES23GH',
    geo: {
      lat: 1.09,
      lng: -0.33
    }
  },
  phone: '+447654899276',
  website: 'debug-masters.com',
  company: {
    name: 'DebugMasters',
    catchPhrase: 'Bug finders of international renown',
    bs: 'Squash more bug, or your money back!'
  }
}]

export const TEST_FUNCTION = () => { return null }