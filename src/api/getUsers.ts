import axios from "axios"

/**
 * Gets 10 users from a fake data set.
 * @returns {User[]} an array of users.
 */
const getUsers = async () => {
  return axios('https://jsonplaceholder.typicode.com/users')
}

export default getUsers