import GeoCoords from "./GeoCoordsModel"

/**
 * Type for Address model.
 */
type Address = {
  street: string,
  suite: string,
  city: string,
  zipcode: string,
  geo: GeoCoords
}

export default Address