import Address from "./AddressModel"
import Company from "./CompanyModel"

/**
 * Interface for User model.
 */
type User = {
  id: number,
  name: string,
  username: string,
  email: string,
  address: Address,
  phone: string,
  website: string,
  company: Company
}

export default User