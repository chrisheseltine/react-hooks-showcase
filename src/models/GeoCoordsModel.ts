/**
 * Interface for GeoCoords model.
 */
type GeoCoords = {
  lat: number,
  lng: number
}

export default GeoCoords