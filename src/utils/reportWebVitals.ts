import { ReportHandler } from 'web-vitals'

/**
 * Measures the performance of the app. Pass a function to handle the logs.
 * @param onPerfEntry the logging function that handles the logs.
 */
const reportWebVitals = (onPerfEntry?: ReportHandler) => {
  if (onPerfEntry && onPerfEntry instanceof Function) {
    import('web-vitals')
      .then(({ getCLS, getFID, getFCP, getLCP, getTTFB }) => {
        getCLS(onPerfEntry)
        getFID(onPerfEntry)
        getFCP(onPerfEntry)
        getLCP(onPerfEntry)
        getTTFB(onPerfEntry)
      })
      .catch(err => console.error(err))
  }
}

export default reportWebVitals
