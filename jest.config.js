module.exports = {
  preset: 'ts-jest',
  testEnvironment: 'js-dom',
  transform: {
    '^.+\\.(ts|tsx)?$': 'ts-jest',
    "^.+\\.(js|jsx)$": "babel-jest",
  },
  transformIgnorePatterns: [
    "node_modules/(?!react-bootstrap)"
  ]
}
